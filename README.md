# philosophers


## Description
This project is based on the "dining philosopher problem", an exercise initially devised by Edsger Dijkstra in  1965 to illustrate the challenges of avoiding deadlock in a computer program. If you want to know a bit more about this, you can take a look at the wikipedia page :
https://en.wikipedia.org/wiki/Dining_philosophers_problem

## Threads and mutexes
In the mandatory part of the project, each philosopher is represented by a thread. The arguments given to the program are :
- the number of philosophers
- the time-frame within which they must eat to avoid dying of starvation ("time to die")
- the amount of time they have to eat ("time to eat")
- the amount of time they have to sleep
- (optional) the maximum number of meal after which the program stops (and nobody dies)

In addition, the subject asks that we implement a "time to think" after they have eaten and slept, although no fixed value is given.

In the bonus part, the subject is basically the same ; however, process are used instead of threads, and semaphores instead of mutexes.

