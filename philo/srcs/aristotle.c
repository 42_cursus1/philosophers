/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aristotle.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/05 15:50:00 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 15:56:16 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

/*
The one and only function allowed to write socrates->stopper
all other times this variable is accessed, are read_only
*/

void	set_stopper(t_socrates *socrates, int check)
{
	pthread_mutex_lock(&socrates->check_finish);
	socrates->stopper = check;
	pthread_mutex_unlock(&socrates->check_finish);
}

/*
If a philo is to die of starvation,
let's have them drink hemlock right away...
or here : set socrates->stopper to 2
*/

int	drink_hemlock(t_socrates *socrates, t_philo *philo)
{
	long long int	timey;

	timey = get_time();
	if ((timey - philo->last_meal) >= socrates->time_to_die)
	{
		usleep(1000);
		set_stopper(socrates, 2);
		print_output(socrates, philo->id, NULL, F_D_RED);
		pthread_mutex_unlock(&philo->meal_time);
		return (1);
	}
	return (0);
}

/*
This is a watcher routine, periodically checks if simul needs to be ended
*/

void	*aristotle(void *arg)
{
	t_socrates	*socrates;

	socrates = (t_socrates *)arg;
	init_wait(socrates);
	while (1)
	{
		if (sim_ender(socrates) == 1)
			return (NULL);
		usleep(1000);
	}
	return (NULL);
}
