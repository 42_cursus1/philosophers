/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:22 by acloos            #+#    #+#             */
/*   Updated: 2023/05/08 10:18:33 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	main(int argc, char **argv)
{
	t_socrates	*socrates;

	socrates = NULL;
	if (argc < 5 || argc > 6)
	{
		ft_error(SYNTAX_ERR);
		return (EXIT_FAILURE);
	}
	if (!check_arg(argv))
		return (EXIT_FAILURE);
	socrates = init_socrates(socrates, argv);
	if (!socrates)
	{
		ft_error("Could not initialize structure\n");
		return (EXIT_FAILURE);
	}
	if (!start_sim(socrates))
	{
		ft_error("Could not start simulation%s\n");
		return (EXIT_FAILURE);
	}
	stop_sim(socrates);
	return (EXIT_SUCCESS);
}
