/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   simulation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/21 16:32:31 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 15:55:57 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

/*
end of simul
stops if stopper == 1 or if stopper ==2
	if stopper == 1, it means everyone ate (at least) their given meals
	if stopper == 2, it means a philo has died (cf fct drink_hemlock)

If stopper had to be set to 1 or 2, function returns 0
*/

int	sim_ender(t_socrates *socrates)
{
	int	i;
	int	all_fed;

	i = 0;
	while (i < socrates->total_phil)
	{
		pthread_mutex_lock(&socrates->philo[i]->meal_time);
		if (drink_hemlock(socrates, socrates->philo[i]) == 1)
			return (1);
		if (socrates->supper_nbr > 0)
		{
			if (socrates->philo[i]->got_meal < socrates->supper_nbr)
				all_fed = 0;
			else
				all_fed = 1;
		}
		pthread_mutex_unlock(&socrates->philo[i]->meal_time);
		i++;
	}
	if (socrates->supper_nbr > 0 && all_fed == 1)
	{
		set_stopper(socrates, 1);
		return (1);
	}
	return (0);
}

/*
returns 1 is simulation can go on, and 0 if it needs to stop
if stopper == 1, simulation has to stop
if stopper == 2, a philo died
*/

int	sim_continue(t_socrates *socrates)
{
	int	sim_status;

	sim_status = 0;
	pthread_mutex_lock(&socrates->check_finish);
	if (socrates->stopper == 1)
		sim_status = 1;
	else if (socrates->stopper == 2)
		sim_status = 2;
	pthread_mutex_unlock(&socrates->check_finish);
	return (sim_status);
}

int	start_sim(t_socrates *socrates)
{
	int		i;

	op_credits(socrates);
	usleep(1000000);
	socrates->start_time = get_time() + (socrates->total_phil * 2 * 10);
	i = 0;
	while (i < socrates->total_phil)
	{
		if (pthread_create(&socrates->philo[i]->philo, NULL, \
			&xanthippe, socrates->philo[i]) != 0)
			return (0);
		i++;
	}
	if (socrates->total_phil > 1)
	{
		if (pthread_create(&socrates->aristotle, NULL, \
			&aristotle, socrates) != 0)
			return (0);
	}
	return (1);
}
