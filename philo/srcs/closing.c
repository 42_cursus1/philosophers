/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   closing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:37 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 15:35:46 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	thread_joiner(t_socrates *socrates)
{
	int	i;

	i = 0;
	while (i < socrates->total_phil)
	{
		pthread_join(socrates->philo[i]->philo, NULL);
		i++;
	}
	if (socrates->total_phil > 1)
		pthread_join(socrates->aristotle, NULL);
}

/*
The following function destroys mutexes,
	depending on how far the program could be initialized/started
	see init_mutexes.c for more info
*/

void	destroyer_of_mutexes(t_socrates *socrates, int mut_check)
{
	int	i;

	i = 0;
	if (mut_check >= 1)
	{
		while (i < socrates->total_phil)
		{
			pthread_mutex_destroy(&socrates->philo[i]->meal_time);
			i++;
		}
	}
	if (mut_check > 1)
	{
		i = 0;
		while (i < socrates->total_phil)
		{
			pthread_mutex_destroy(&socrates->forks[i]);
			i++;
		}
	}
	if (mut_check >= 2)
		pthread_mutex_destroy(&socrates->check_finish);
	if (mut_check >= 3)
		pthread_mutex_destroy(&socrates->printer);
}

void	*free_philos(t_socrates *socrates)
{
	int	i;

	i = 0;
	if (!socrates)
		return (NULL);
	if (socrates->forks != NULL)
		free(socrates->forks);
	if (socrates->philo != NULL)
	{
		while (i < socrates->total_phil)
		{
			if (socrates->philo[i])
				free(socrates->philo[i]);
			i++;
		}
		free(socrates->philo);
	}
	free(socrates);
	return (NULL);
}

void	stop_sim(t_socrates *socrates)
{
	thread_joiner(socrates);
	destroyer_of_mutexes(socrates, 4);
	free_philos(socrates);
}
