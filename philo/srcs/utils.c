/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/21 11:42:49 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 15:40:49 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

long	ft_atol(const char *nptr)
{
	int		i;
	long	sign;
	long	res;

	i = 0;
	sign = 1;
	res = 0;
	while (nptr[i] >= '0' && nptr[i] <= '9')
		res = res * 10 + nptr[i++] - '0';
	if (res > 2147483647 || res < -2147483648)
		return (-1);
	return (sign * (int)res);
}

/*
The last arg is the ansi color code as defined in the ansi_colors.h file

locks/unlocks mutex to print output on terminal
the char *str is what is to be printed
printf : [timestamp_in_ms] [philo_id] [string]
possible strings for output :
	has taken a fork
	is eating
	is sleeping
	is thinking
	died

*/

void	print_output(t_socrates *socrates, int id, char *str, char *a)
{
	pthread_mutex_lock(&socrates->printer);
	if (!str)
	{
		str = "died";
		printf("%s[%ld] [%d] [%s]%s\n", F_D_RED, \
			(get_time() - socrates->start_time), id + 1, str, RST);
	}
	else if (sim_continue(socrates) != 1 && sim_continue(socrates) != 2)
		printf("%s[%ld] [%d] [%s]%s\n", a, \
			(get_time() - socrates->start_time), id + 1, str, RST);
	pthread_mutex_unlock((&socrates->printer));
}

/*
This function will print the string message given as arg in case of error
*/

int	ft_error(char *err_msg)
{
	printf("\n%s%s%sError!%s\n\n", BOLD, F_L_YELLOW, B_D_RED, RST);
	printf("%s%s%s\n%s", ITALICS, F_D_YELLOW, err_msg, RST);
	return (0);
}

/*
Just a welcome banner
*/

void	op_credits(t_socrates *socrates)
{
	char	*sim;
	int		i;

	i = 0;
	sim = "\tSTARTING SIMULATION ... !\n\n";
	printf("\n\n%s%s", BOLD, F_L_GREEN);
	printf("%-23s : %6d\n", "Number of philosophers", socrates->total_phil);
	usleep(500000);
	printf("%-23s : %6ld\n", "Time to die", socrates->time_to_die);
	usleep(500000);
	printf("%-23s : %6ld\n", "Time to eat", socrates->time_to_eat);
	usleep(500000);
	printf("%-23s : %6ld\n", "Time to sleep", socrates->time_to_sleep);
	if (socrates->supper_nbr > 0)
		printf("%-23s : %6d", "Maximum number of meals", socrates->supper_nbr);
	else
		printf("%-23s : %6s", "Maximum number of meals", "N/A");
	printf("%s%s%s%s\n\n\n", RST, BOLD, ITALICS, F_L_MAGENTA);
	while (sim[i])
	{
		usleep(100000);
		write(1, &sim[i++], 1);
	}
	printf("%s", RST);
}
