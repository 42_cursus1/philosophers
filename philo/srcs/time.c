/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:08 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 11:43:40 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

/*
The structure used by the function gettimeofday() stores time 
	in seconds and micro-seconds
The following function helps convert time in milli-seconds
*/

long int	get_time(void)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	return ((time.tv_sec * 1000) + (time.tv_usec / 1000));
}

/*
This function calculates until when a philo stays in a given state
also periodically checks if simulation has stopped
*/

void	philo_time(int time_to, t_socrates *socrates)
{
	long int	start;

	start = get_time();
	while ((time_to + start) > get_time())
	{
		if (sim_continue(socrates) == 1 || sim_continue(socrates) == 2)
			break ;
		usleep(1000);
	}
}

/*
This function creates a small delay at the beginning of both main routines
This allows for all threads to start at the same time
*/

void	init_wait(t_socrates *socrates)
{
	while (get_time() < socrates->start_time)
		continue ;
}
