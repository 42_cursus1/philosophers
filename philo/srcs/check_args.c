/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:33 by acloos            #+#    #+#             */
/*   Updated: 2023/03/29 11:22:05 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_isdigit(int c)
{
	if (c < '0' || c > '9')
		return (0);
	return (1);
}

int	check_digits(char **argv)
{
	int	i;
	int	j;

	i = 1;
	j = 0;
	while (argv[i])
	{
		while (argv[i][j])
		{
			if (!ft_isdigit(argv[i][j]))
			{
				ft_error("Input must be a number");
				return (0);
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (1);
}

int	check_arg(char **argv)
{
	if (!check_digits(argv))
		return (0);
	if (argv[1][0] == '-' || argv[1][0] == '0' || argv[1][0] == 0)
	{
		ft_error("You must have at least 1 philosopher");
		return (0);
	}
	if (argv[2][0] == '-' || argv[3][0] == '-' || argv[4][0] == '-')
	{
		ft_error("The duration times cannot be negative");
		return (0);
	}
	else if (argv[2][0] == 0 || argv[3][0] == 0 || argv[4][0] == 0)
	{
		ft_error("All duration times need to be specified");
		return (0);
	}
	if (argv[5] && (argv[5][0] == '-' || argv[5][0] == '0' || argv[5][0] == 0))
	{
		ft_error("Philosophers need to eat at least once");
		return (0);
	}
	return (1);
}
