/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_mutexes.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:17 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 10:43:23 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

pthread_mutex_t	*fork_mutex_init(t_socrates *socrates)
{
	pthread_mutex_t	*forks;
	int				i;

	i = 0;
	forks = malloc(sizeof(pthread_mutex_t) * socrates->total_phil);
	if (!forks)
		return (NULL);
	while (i < socrates->total_phil)
	{
		if (pthread_mutex_init(&forks[i], NULL) != 0)
			return (NULL);
		i++;
	}
	return (forks);
}

int	init_mutexes(t_socrates *socrates)
{
	int	mut_check;

	mut_check = 0;
	socrates->forks = fork_mutex_init(socrates);
	if (!socrates->forks)
		mut_check = 1;
	if (pthread_mutex_init(&socrates->check_finish, NULL) != 0)
		mut_check = 2;
	if (pthread_mutex_init(&socrates->printer, NULL) != 0)
		mut_check = 3;
	if (mut_check > 0)
	{
		ft_error("Could not initialize mutexes");
		destroyer_of_mutexes(socrates, mut_check);
		free_philos(socrates);
		return (0);
	}
	return (1);
}
