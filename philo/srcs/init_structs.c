/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_structs.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:26 by acloos            #+#    #+#             */
/*   Updated: 2023/05/03 17:59:00 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

/*
The 'i' counter in init_philo is set to 0, although philos start with '1'
	-> when printing output, will print : id + 1
Starting at 0 allows for the left/right formula below
*/

void	assign_forks(t_philo *philo)
{
	philo->right_fork = philo->id;
	philo->left_fork = (philo->id + 1) % philo->socrates->total_phil;
	if (philo->id % 2)
	{
		philo->right_fork = (philo->id + 1) % philo->socrates->total_phil;
		philo->left_fork = philo->id;
	}
}

/*
Creates arrays for all philos
*/

t_philo	**init_philos(t_socrates *socrates)
{
	t_philo	**philo;
	int		i;

	i = 0;
	philo = malloc(sizeof(t_philo) * socrates->total_phil);
	if (!philo)
	{
		ft_error("Could not initialize philos");
		return (NULL);
	}
	while (i < socrates->total_phil)
	{
		philo[i] = malloc(sizeof(t_philo) * 1);
		if (!philo[i])
			return (NULL);
		if (pthread_mutex_init(&philo[i]->meal_time, 0) != 0)
			return (NULL);
		philo[i]->socrates = socrates;
		philo[i]->id = i;
		philo[i]->got_meal = 0;
		philo[i]->last_meal = 0;
		assign_forks(philo[i]);
		i++;
	}
	return (philo);
}

/*
my ft_atol function returns -1 for a number bigger than an int
*/

int	check_soc(t_socrates *socrates)
{
	if (socrates->total_phil < 0 || socrates->time_to_die < 0 || \
		socrates->time_to_eat < 0 || socrates->time_to_sleep < 0 || \
		socrates->supper_nbr == -1)
	{
		ft_error("Input must fit within sizeof(int)");
		free(socrates);
		return (0);
	}
	return (1);
}

/*
initializes all struct vars, except :
	start_time (in start_sims)
*/

t_socrates	*init_socrates(t_socrates *socrates, char **argv)
{
	socrates = (t_socrates *)malloc(sizeof(t_socrates) * 1);
	if (!socrates)
		return (0);
	socrates->total_phil = ft_atol(argv[1]);
	socrates->time_to_die = ft_atol(argv[2]);
	socrates->time_to_eat = ft_atol(argv[3]);
	socrates->time_to_sleep = ft_atol(argv[4]);
	if (argv[5])
		socrates->supper_nbr = ft_atol(argv[5]);
	else
		socrates->supper_nbr = -2;
	socrates->stopper = 0;
	if (!check_soc(socrates))
		return (0);
	socrates->philo = init_philos(socrates);
	if (!socrates->philo)
		return (0);
	if (!init_mutexes(socrates))
		return (0);
	return (socrates);
}
