/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:13 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 15:56:02 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

/*
Creates a delay at the start of the main routine for half philos
This limits the risk of deadlock when reaching for the first fork
*/

void	hypnos(t_socrates *socrates)
{
	philo_time(socrates->time_to_sleep, socrates);
}

/*
This is the thinking routine
*/

void	rodin(t_socrates *socrates, t_philo *philo)
{
	long int	time_to_think;

	pthread_mutex_lock(&philo->meal_time);
	time_to_think = (philo->socrates->time_to_die - \
		(get_time() - philo->last_meal) - philo->socrates->time_to_eat) / 2;
	pthread_mutex_unlock(&philo->meal_time);
	if (time_to_think < 0)
		time_to_think = 0;
	print_output(socrates, philo->id, "is thinking", F_D_GREEN);
	philo_time(time_to_think, socrates);
}

/*
This is the Eat & sleep routine
The hemlock will help kill them if they are to die of starvation
The meal check is to verify if they had all of their meals
*/

void	banquet(t_socrates *socrates, t_philo *philo)
{
	pthread_mutex_lock(&socrates->forks[philo->right_fork]);
	print_output(socrates, philo->id, "has taken a fork", F_L_MAGENTA);
	pthread_mutex_lock(&socrates->forks[philo->left_fork]);
	print_output(socrates, philo->id, "has taken a fork", F_D_MAGENTA);
	print_output(socrates, philo->id, "is eating", F_L_YELLOW);
	pthread_mutex_lock(&philo->meal_time);
	philo->last_meal = get_time();
	pthread_mutex_unlock(&philo->meal_time);
	philo_time(socrates->time_to_eat, socrates);
	if (sim_continue(socrates) == 0)
	{
		pthread_mutex_lock(&philo->meal_time);
		philo->got_meal++;
		pthread_mutex_unlock(&philo->meal_time);
	}
	print_output(socrates, philo->id, "is sleeping", F_D_CYAN);
	pthread_mutex_unlock(&socrates->forks[philo->left_fork]);
	pthread_mutex_unlock(&socrates->forks[philo->right_fork]);
	philo_time(socrates->time_to_sleep, socrates);
}

/*
Special function in case of just one philo
Because without it, program won't stop
(and didn't feel like starting from scratch - again)
*/

void	*alone_in_the_dark(t_socrates *socrates, t_philo *philo)
{
	pthread_mutex_lock(&socrates->forks[philo->right_fork]);
	print_output(socrates, philo->id, "has taken a fork", F_L_MAGENTA);
	philo_time(socrates->time_to_die, socrates);
	print_output(socrates, philo->id, NULL, F_D_RED);
	pthread_mutex_unlock(&socrates->forks[philo->right_fork]);
	return (NULL);
}

/*
During the while loop :
	- check that the simul is ongoing (check_finish == 0)
	- start eating "banquet" routine
	- then sleep routine
	- then think routine
*/

void	*xanthippe(void *arg)
{
	t_philo		*philo;
	t_socrates	*socrates;

	philo = (t_philo *)arg;
	socrates = philo->socrates;
	pthread_mutex_lock(&philo->meal_time);
	philo->last_meal = philo->socrates->start_time;
	pthread_mutex_unlock(&philo->meal_time);
	init_wait(socrates);
	if (socrates->total_phil == 1)
		return (alone_in_the_dark(socrates, philo));
	else if (philo->id % 2)
		hypnos(socrates);
	while (sim_continue(socrates) == 0)
	{
		banquet(socrates, philo);
		rodin(socrates, philo);
	}
	return (NULL);
}
