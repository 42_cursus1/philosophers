/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:50:51 by acloos            #+#    #+#             */
/*   Updated: 2023/05/05 15:56:40 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include "ansi_colors.h"
# include "linux_keys.h"
# include <string.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/time.h>
# include <pthread.h>

# define SYNTAX_ERR	"Syntax error detected ! \
Please use as :\n\n\t\t./philo <nbr> <time_to_die> <time_to_eat> \
<time_to_sleep> <number_of_times_each_philosopher_must_eat>\n"

/******************************************************************************
*                                 Structures                                  *
******************************************************************************/

typedef struct s_socrates	t_socrates;

typedef struct s_philo
{
	pthread_t			philo;
	pthread_mutex_t		meal_time;
	int					id;
	int					right_fork;
	int					left_fork;
	int					got_meal;
	long int			last_meal;
	struct s_socrates	*socrates;
}	t_philo;

typedef struct s_socrates
{
	pthread_mutex_t		*forks;
	pthread_mutex_t		check_finish;
	pthread_mutex_t		printer;
	pthread_t			aristotle;
	int					total_phil;
	long int			time_to_die;
	long int			time_to_eat;
	long int			time_to_sleep;
	int					supper_nbr;
	int					stopper;
	long int			start_time;
	t_philo				**philo;
}	t_socrates;

/******************************************************************************
*                      Function Prototypes & Declaration                      *
******************************************************************************/

//aristotle.c
void			set_stopper(t_socrates *socrates, int check);
int				drink_hemlock(t_socrates *socrates, t_philo *philo);
void			*aristotle(void *arg);

//check_args.c
int				ft_isdigit(int c);
int				check_digits(char **argv);
int				check_arg(char **argv);

//closing.c
void			thread_joiner(t_socrates *socrates);
void			destroyer_of_mutexes(t_socrates *socrates, int mut_check);
void			*free_philos(t_socrates *socrates);
void			stop_sim(t_socrates *socrates);

//init_mutexes.c
int				init_mutexes(t_socrates *socrates);
pthread_mutex_t	*fork_mutex_init(t_socrates *socrates);

//init_structs.c
t_socrates		*init_socrates(t_socrates *socrates, char **argv);
int				check_soc(t_socrates *socrates);
t_philo			**init_philos(t_socrates *socrates);
void			assign_forks(t_philo *philo);

//routine.c
void			*xanthippe(void *arg);
void			banquet(t_socrates *socrates, t_philo *philo);
void			rodin(t_socrates *socrates, t_philo *philo);
void			hypnos(t_socrates *socrates);
void			*alone_in_the_dark(t_socrates *socrates, t_philo *philo);

//simulation.c
int				start_sim(t_socrates *socrates);
int				sim_continue(t_socrates *socrates);
int				sim_ender(t_socrates *socrates);

//time.c
long int		get_time(void);
void			philo_time(int time_to, t_socrates *socrates);
void			init_wait(t_socrates *socrates);

//utils.c
long			ft_atol(const char *nptr);
int				ft_error(char *err_msg);
void			print_output(t_socrates *socrates, int id, char *str, char *a);
void			op_credits(t_socrates *socrates);

#endif
